## Exercices sur GIT et VIM

Tous les TD et TP de ce modules sont à faire sur une machine virtuelle. Vous pouvez récupérer une
debian minimal sur rt-cloud. Attention, vous n'avez pas besoin d'interface graphique dans votre machine
virtuelle. Votre système hôte vous le fournis.

#### TD1:

- [GIT](https://gitlab.com/m3206/TD/blob/master/GIT.md)
- [VIM](https://gitlab.com/m3206/TD/blob/master/VIM.md)


