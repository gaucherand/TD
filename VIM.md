## Exercices sur VIM

Dans les exercices suivant le plus important n'est pas seulement d'effectuer ce qui est demandé, mais de l'effectuer avec le minimum de touches possible. 
Vous ne devez pas utiliser la souris.
Vous devez envoyer la séquence de touche que vous avez utiliser et leur signification.

Par exemple:
```bash
$ vim test1 (lancement de l'edittion du fichier test1)
jjj (pour descendre de 3 lignes)
i (passer en mode insertion)
toto est malade (insérer le texte "toto est malade")
<esc>:wq (pour passer en mode édition, sauvegarder et quitter)
$
```


## Exercice 01 (files/exo_01.txt)
`wget https://gitlab.com/m3206/TD/raw/master/files/exo_01.txt`

transformer: 

```bash
*temp var1 0
*temp var2 "hi"
*temp var3 -1
*temp var4 42
*temp var5 "asdf"
*temp var6 0

Simple things we do all the time should be able to be done with very few keystrokes, 
but sometimes I find something I need to do makes me go, "There MUST be a better way."

This challenge is just a simple movement and entering text at a certain place.
```

en:

```bash
*temp var1 0
*temp var2 "hi"
*temp var3 -1
*temp var4 42
*temp var5 "asdf"
*temp var6 0
*temp var7 11

Simple things we do all the time should be able to be done with very few keystrokes, 
but sometimes I find something I need to do makes me go, "There MUST be a better way."

New text.

This challenge is just a simple movement and entering text at a certain place.
```

## Exercice 02 (files/exo_02.txt)
`wget https://gitlab.com/m3206/TD/raw/master/files/exo_02.txt`

Dans le fichier, remplacer `Sed` par `XXX` et `sed` par `xxx`


## Exercice 03 (files/exo_03.txt)
`wget https://gitlab.com/m3206/TD/raw/master/files/exo_03.txt`

Transformer: 

```python
class Golfer
     def initialize; end # initialize
  def useless; end;
  
     def self.hello(a,b,c,d)
      puts "Hello #{a}, #{b}, #{c}, #{d}"
   end
end
```

en: 
```python
class Golfer
  def self.hello(*a)
    puts "Hello #{a.join(',')}"
  end
end
```

## Exercice 04 (files/exo_04.txt)
`wget https://gitlab.com/m3206/TD/raw/master/files/exo_04.txt`

Le fichier contient 99 'V', il faut le transformer en 100 'i'



## Allez plus loin:
- https://blog.interlinked.org/tutorials/vim_tutorial.html
- vimtutor
- http://www.openvim.com/tutorial.html